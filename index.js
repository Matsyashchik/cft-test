const searchBox = document.getElementsByClassName("search-txt").item(0);
const searchResult = document.getElementsByClassName("search-result").item(0);
const table = document.getElementById("table-content");
let paintedCells = [];

function searchCoincidences() {
    const search = searchBox.value;
    if (!!search)
    {
        clearSearch();
        let coincidencesCount = findCoincidences(search)
        showSearchResult(coincidencesCount);
    } else {
        alert("Введите значение для поиска");
    }
}

function findCoincidences (pattern) {
    let regexp = new RegExp(pattern, "i");
    let coincidencesCount = 0;
    for (let i = 0; i < table.children.length; i++) {
        let cell = table.children[i].children[2];
        if (regexp.test(cell.textContent)) {
            paintTableCell(cell);
            coincidencesCount++;
        }
    }
    return coincidencesCount;
}

function clearSearch () {
    searchResult.hidden = true;
    for (const cell of paintedCells) {
        cell.classList.remove("coincidence")
    }
    paintedCells = [];
}

function showSearchResult (coincidencesCount) {
    searchResult.hidden = false;
    searchResult.firstChild.textContent = coincidencesCount === 0
        ? "Ничего не найдено"
        : "Колличество совпадений: " + coincidencesCount;
}

function paintTableCell (cell) {
    paintedCells.push(cell);
    cell.classList.add("coincidence");
}

